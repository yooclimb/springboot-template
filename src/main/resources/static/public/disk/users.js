$('#user-table tbody .delete-btn').on('click', function() {
	var tr = $(this).parents('tr')
	var td = tr.find('.user-id')
	var id = td.text();
	layer.confirm('确定删除账户？该操作会同时删除该用户的所有文件', {
		btn : [ '确定', '取消' ]
	//按钮
	}, function() {
		window.location.href = 'user/delete?id=' + id
	});
});

$('#user-add-btn').on('click', function() {
	layer.open({
		type : 1,
		shade : false,
		title : '添加用户', //不显示标题.
		area : [ '480px', '275px' ],
		content : $('#add-user-pop')
	});
})