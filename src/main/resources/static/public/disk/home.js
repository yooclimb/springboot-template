layui.use([ 'jquery','layer', 'form', 'element' ,'upload'], function() {
	var layer = layui.layer, form = layui.form, element = layui.element()
	
	$('#logout-btn').on('click', function() {
		layer.confirm('确定注销并返回登录界面？', {
			btn : [ '确定', '取消' ]
		// 按钮
		}, function() {
			window.location.href = 'logout'
		});
	})

	$('#up-btn').on('click', function() {
		layer.open({
			type : 1,
			shade : false,
			title : '文件上传', // 不显示标题.
			area : [ '580px', '355px' ],
			content : $('#up-pop')
		});
	})

	$('#repass-btn').on('click', function() {
		layer.open({
			type : 1,
			shade : false,
			title : '修改密码', // 不显示标题.
			area : [ '480px', '275px' ],
			content : $('#re-pass-pop')
		});
	})

	$('#repass-sub-btn').on('click', function() {
		var o_pass = $('#o_pass').val();
		var n_pass = $('#n_pass').val();
		var re_pass = $('#re_pass').val();
		if (n_pass != re_pass) {
			layer.msg('两次输入的新密码不一致，请重新输入');
		} else {
			$.post('repass', {
				'o_pass' : o_pass,
				'n_pass' : n_pass
			}, function(res) {
				if (res) {
					layer.closeAll();
					layer.msg('密码修改成功');
				} else {
					layer.msg('输入原密码错误，请重新输入');
				}
			})
		}
	})
});

