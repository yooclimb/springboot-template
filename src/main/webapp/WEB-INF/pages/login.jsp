<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>欢迎使用</title>
<link type="text/css" href="public/layui/css/layui.css" rel="stylesheet">
<link type="text/css" href="public/datatable/jquery.dataTables.min.css"
	rel="stylesheet">
</head>

<body
	style="background-image: url(public/image/body_bg.jpg); background-size: cover;">
	<div class="layui-layout layui-layout-admin"
		style="width: 80%; margin: auto; min-width: 1000px; margin-top: 150px;">
		<div id="left" style="width: 500px; float: left">
			<img src="public/image/login.png" alt="" width="500px">
		</div>
		<div id="right" style="width: 500px; float: right; margin-top: 60px">
			<form class="layui-form" action="check" method="post">
				<div class="layui-form-item">
					<fieldset class="layui-elem-field layui-field-title">
						<legend style="color: #009688; font-weight: 900;">嗨云盘，请登录</legend>
					</fieldset>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">帐号:</label>
					<div class="layui-input-block">
						<input type="text" name="account" lay-verify="title"
							autocomplete="off" placeholder="请输入帐号" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">密码:</label>
					<div class="layui-input-block">
						<input type="password" name="password" lay-verify="title"
							autocomplete="off" placeholder="请输入密码" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">登录</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<span style="color: #FF5722">${message}</span>
					</div>
				</div>

			</form>

		</div>
	</div>
</body>
<script src="public/jquery.js"></script>
<script src="public/layui/layui.js"></script>
<script src="public/datatable/jquery.dataTables.min.js"></script>
<script src="public/disk/home.js"></script>
</html>