<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>网盘首页</title>
<link type="text/css" href="public/layui/css/layui.css" rel="stylesheet">
<link type="text/css" href="public/datatable/jquery.dataTables.min.css"
	rel="stylesheet">
</head>
<body>
	<div class="layui-layout layui-layout-admin">
		<ul class="layui-nav" style="text-align: right">
			<li class="layui-nav-item" style="float: left;"><a
				href="javascript:;"> LOGO </a></li>
			<li class="layui-nav-item" style="float: left;"><a
				href="javascript:;">
					<button class="layui-btn" id="up-btn" style="margin-top: 10px">上传</button>
			</a></li>
			<li class="layui-nav-item layui-this"><a href="javascript:;">
					${sessionScope.user.account } </a></li>
			<li class="layui-nav-item "><a href="javascript:;"
				id="repass-btn">修改密码</a></li>
			<c:if test="${sessionScope.user.admin }">
				<li class="layui-nav-item"><a href="user">用户管理</a></li>
			</c:if>

			<li class="layui-nav-item" id="logout-btn"><a
				href="javascript:;">登出</a></li>
		</ul>
		<div class="layui-side layui-bg-black">
			<div class="layui-side-scroll">
				<ul class="layui-nav layui-nav-tree site-demo-nav">
					<li class="layui-nav-item layui-this"><a href="home">全部文件</a></li>
					<li class="layui-nav-item"><a href="home/docs">文本</a></li>
					<li class="layui-nav-item"><a href="home/images">图片</a></li>
					<li class="layui-nav-item"><a href="home/audios">音频</a></li>
					<li class="layui-nav-item"><a href="home/videos">视频</a></li>
					<li class="layui-nav-item"><a href="home/others">其他</a></li>
					<li class="layui-nav-item" style="height: 30px; text-align: center"></li>
				</ul>
			</div>
		</div>
		<div class="layui-tab layui-tab-brief" lay-filter="demoTitle">
			<div class="layui-body layui-tab-content site-demo site-demo-body"
				style="margin-top: 60px">TODO：这里放置统计图表</div>
		</div>
	</div>

	<div id="up-pop" style="display: none; padding: 10px">

		<form class="layui-form" action="">
			<div class="layui-form-item">
				<label class="layui-form-label">上传文件</label>
				<div class="layui-input-block">
					<input type="text" name="title" lay-verify="title"
						autocomplete="off" placeholder="请输入标题" class="layui-input">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">数据类型</label>
				<div class="layui-input-block">
					<select name="interest" lay-filter="aihao">
						<option value="0" selected="">文本</option>
						<option value="1" >图片</option>
						<option value="2">音频</option>
						<option value="3">视频</option>
						<option value="4">其他</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">智能类型</label>
				<div class="layui-input-block">
					<input type="checkbox" checked="" name="open" lay-skin="switch"
						lay-filter="switchTest" title="开关">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div>

	<div id="re-pass-pop" style="display: none; padding: 10px">
		<div class="layui-form-item">
			<label class="layui-form-label">原密码</label>
			<div class="layui-input-block">
				<input type="password" name="o_pass" id="o_pass" lay-verify="title"
					autocomplete="off" placeholder="请输入原密码" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">新密码</label>
			<div class="layui-input-block">
				<input type="password" name="n_pass" id="n_pass" lay-verify="title"
					autocomplete="off" placeholder="请输入新密码" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">再次输入</label>
			<div class="layui-input-block">
				<input type="password" name="title" id="re_pass" lay-verify="title"
					autocomplete="off" placeholder="请再次输入新密码" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" lay-submit="" id="repass-sub-btn"
					lay-filter="demo1">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		</div>
	</div>

</body>
<script src="public/jquery.js"></script>
<script src="public/datatable/jquery.dataTables.min.js"></script>
<script src="public/layui/layui.js"></script>
<script src="public/disk/home.js"></script>

</html>