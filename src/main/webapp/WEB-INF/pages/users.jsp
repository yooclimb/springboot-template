<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>欢迎使用</title>
<link type="text/css" href="public/layui/css/layui.css" rel="stylesheet">
<link type="text/css" href="public/datatable/jquery.dataTables.min.css"
	rel="stylesheet">
</head>
<body>
	<div class="layui-layout layui-layout-admin">
		<ul class="layui-nav" style="text-align: right">
			<li class="layui-nav-item" style="float: left;"><a
				href="javascript:;"> LOGO </a></li>
			<li class="layui-nav-item layui-this">${sessionScope.user.account }
			</li>
			<li class="layui-nav-item" id="logout-btn"><a
				href="javascript:;">登出</a></li>
		</ul>
		<div class="layui-tab layui-tab-brief" lay-filter="demoTitle"
			style="width: 90%; margin: auto; margin-top: 20px">
			<button class="layui-btn" id="user-add-btn">添加用户</button>
			<table class="layui-table" id="user-table">
				<thead>
					<tr>
						<th>用户ID</th>
						<th>用户名</th>
						<th>角色</th>
						<th width="100px">删除用户</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users }" var="user">
						<tr>
							<td class="user-id">${user.id}</td>
							<td>${user.account }</td>
							<td><c:if test="${user.admin }">
							管理员</c:if> <c:if test="${!user.admin }">
							普通用户</c:if></td>
							<td><button class="layui-btn layui-btn-danger delete-btn">删除</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<div id="add-user-pop" style="display: none; padding: 10px">
		<form class="layui-form" action="user/add" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input type="text" name="account" lay-verify="title"
						autocomplete="off" placeholder="请输入标题" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">密码</label>
				<div class="layui-input-block">
					<input type="text" name="password" lay-verify="title"
						autocomplete="off" placeholder="请输入标题" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">角色</label>
				<div class="layui-input-block">
					<input type="radio" name="admin" value="false" title="普通用户" checked>
					<input type="radio" name="admin" value="true" title="管理员">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div>
</body>
<script src="public/jquery.js"></script>
<script src="public/datatable/jquery.dataTables.min.js"></script>
<script src="public/layui/layui.js"></script>
<script src="public/disk/users.js"></script>
<script type="text/javascript">
	layui.use([ 'layer', 'form' ], function(args) {
	});
</script>
</html>
